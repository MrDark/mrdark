package com.mrdark;

import com.mrdark.dao.UserMapper;
import com.mrdark.domain.User;
import com.mrdark.service.IUsrService;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MrdarkApplicationTests {
    @Autowired(required = false)
    UserMapper userMapper;
    @Autowired
    IUsrService iUsrService;
    @Test
    public void contextLoads() {
    }

    @Test
    public void selectUser(){
        System.out.println(userMapper.selectUserByUserid("1253741739@qq.com"));
    }

    @Test
    public void getPass(){
        ByteSource byteSourceSalt = ByteSource.Util.bytes("2391149542@qq.com");
        Object passObj = new SimpleHash("MD5", "123456", byteSourceSalt, 2);
        System.out.println(passObj.toString());

    }
    @Test
    public void addUser() {
        User user = new User();
        user.setUserid("123456782@qq.com");
        user.setUsername("小东");
        user.setPassword("123456");
        user.setSex("1");
        user.setAge("23");
        user.setUserflg("1");
        System.out.println(iUsrService.addUser(user));
        //System.out.println(userMapper.insert(user));
    }
}
