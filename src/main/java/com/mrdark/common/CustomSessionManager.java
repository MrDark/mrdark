package com.mrdark.common;

import com.mrdark.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;

@Slf4j
@Service
public class CustomSessionManager extends DefaultWebSessionManager {

    private static final String token = "token";
    private static final String REFERENCED_SESSION_ID_SOURCE = "Stateless request";

    public CustomSessionManager() {
        super();
        setGlobalSessionTimeout(DEFAULT_GLOBAL_SESSION_TIMEOUT * 48);
    }

    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {

        String id = WebUtils.toHttp(request).getHeader(token);
        log.info("请求上下文" + request.getLocalAddr());
        if (null == id || id.isEmpty()) {
            id = "xxxxxxxxxxxxxxxx";//放入一个错误的
            //throw new MyException("3000","请求信息未带上认证临牌，非法请求！");

        }else {
            try {
                JwtUtil.User user = JwtUtil.decode(id, JwtUtil.User.class);
                log.info("解密token得到>>>>" + user.toString());
                id = user.getToken();
            }catch (Exception e){
                id = "xxxxxxxxxxxxxxxx";//放入一个错误的
                //throw new MyException("3000","请求令牌非法！");

            }

        }
        request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, REFERENCED_SESSION_ID_SOURCE);
        request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, id);
        request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);
        return id;

    }

}
