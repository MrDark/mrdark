package com.mrdark.common;

import com.auth0.jwt.internal.com.fasterxml.jackson.core.JsonParseException;
import com.mrdark.vo.Rst;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常拦截
 */
@ControllerAdvice
public class AllExceptionHandler {

    /**
     * 自定义异常
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(MyException.class)
    public Rst MyException(MyException e){
        return new Rst("3000",e.getMessage());
    }

    /**
     * 自定义异常
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(JsonParseException.class)
    public Rst JsonParseException(Exception e){
        return new Rst("3000","非法令牌");
    }


}
