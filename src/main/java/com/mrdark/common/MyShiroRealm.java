package com.mrdark.common;

import com.mrdark.domain.Permission;
import com.mrdark.domain.Role;
import com.mrdark.domain.User;
import com.mrdark.service.IRoleAndPermissionService;
import com.mrdark.service.IUsrService;
import com.mrdark.service.impl.IRoleAndPermissionServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@Slf4j
public class MyShiroRealm extends AuthorizingRealm {
    @Autowired(required = false)
    IUsrService iUsrService;
    @Autowired
    private IRoleAndPermissionService iRoleAndPermissionService;


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        //log.info("----------doGetAuthorizationInfo方法被调用----------");
        String username = (String) getAvailablePrincipal(principals);
        //通过用户名从数据库获取权限字符串

        ////角色
        User user = iUsrService.selectUserByUserid(username);
        List<Role> roles = iRoleAndPermissionService.selectRoleByRoleid(username);
        Set<String> roleSet = new HashSet<String>();
        for (Role role: roles){
            roleSet.add(role.getRoleid());
        }
        //权限
        List<Permission> permissions = iRoleAndPermissionService.selectPermissionByRole(roles);
        Set<String> permissionsSet = new HashSet<String>();//权限
        for (Permission permission:permissions){
            permissionsSet.add(permission.getPermissionkey());
        }
        log.info("角色>>>" + roleSet.toString());
        log.info("权限>>>" + permissionsSet.toString());

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permissionsSet);
        info.setRoles(roleSet);
        return info;
    }
    /**
     * 身份校验通过返回一个token
     *登陆认证，登陆时调用
     * @param authcToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {

        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
        String userId = token.getUsername();

        User user = iUsrService.selectUserByUserid(userId);

        if (user == null ){
            throw new AccountException("无此账户，请确认账户是否输入正确！");
        }
        else if ("0".equals(user.getUserflg())){
            throw new AccountException("此账号为冻结状态无法登陆，请联系管理人员！");
        }
        else{
            //加盐
            ByteSource byteSourceSalt = ByteSource.Util.bytes(user.getUserid());
            //校验密码
//            Object obj = new SimpleHash("MD5", user.getPassword(), byteSourceSalt, 2);

            SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                    user.getUserid(),//用户账户名
                    user.getPassword(),//用户密码
                    byteSourceSalt,//加盐
                    getName()
            );
            return authenticationInfo;

        }
    }

}
