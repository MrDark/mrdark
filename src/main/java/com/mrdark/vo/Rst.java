package com.mrdark.vo;

import java.util.Map;

public class Rst {
    public String code;
    public String msg;
    public Object data;

    public Rst(String code,String msg,Map data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Rst(String code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public void setRst(String code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Rst{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
