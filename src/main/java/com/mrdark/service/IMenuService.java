package com.mrdark.service;

import com.mrdark.vo.Rst;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface IMenuService {
    /**
     * 根据用户id 获取菜单和动态路由
     * @param userid
     * @return
     */
    Rst selectMenuAndRoute(String userid);
}
