package com.mrdark.service;

import com.mrdark.domain.Menu;
import com.mrdark.domain.User;
import com.mrdark.vo.Rst;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface IUsrService {

     /**
      * 登录
      * @param user
      * @return
      */
     Rst loginUser(User user);

     /**
      * 添加用户
      * @param user
      * @return
      */
     Rst addUser(User user);

     /**
      * 删除用户
      * @param userId
      * @return
      */
     Rst deleteUser(String userId);

     /**
      * 修改用户信息
      * @param user
      * @return
      */
     Rst updateUser(User user);

     /**
      * 根据用户id查询用户
      * @param userId
      * @return
      */
     User selectUserByUserid(String userId);




}
