package com.mrdark.service.impl;

import com.mrdark.dao.PermissionMapper;
import com.mrdark.dao.RoleMapper;
import com.mrdark.dao.RolePermissionMapper;
import com.mrdark.dao.UsrRoleMapper;
import com.mrdark.domain.Permission;
import com.mrdark.domain.Role;
import com.mrdark.domain.RolePermission;
import com.mrdark.domain.UsrRole;
import com.mrdark.service.IRoleAndPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Service("IRoleAndPermissionService")
public class IRoleAndPermissionServiceImpl implements IRoleAndPermissionService {
    @Autowired(required = false)
    RoleMapper roleMapper;
    @Autowired(required = false)
    PermissionMapper permissionMapper;
    @Autowired(required = false)
    UsrRoleMapper usrRoleMapper;
    @Autowired(required = false)
    RolePermissionMapper rolePermissionMapper;


    public List<Role> selectRoleByRoleid(String userid){
       List<UsrRole> usrRoles =  usrRoleMapper.selectRoleidByUserid(userid);
       log.info("usrRole= " + usrRoles.toString());
       List<String> paramList = new ArrayList<>();
       for (UsrRole usrRole: usrRoles){
           log.info(usrRole.toString());
           paramList.add(usrRole.getRoleid());
       }
        log.info("paramList = " + paramList);
        return  roleMapper.selectRoleByRoleid(paramList);
    }

    public List<Permission> selectPermissionByRole(List<Role> roles){
            List<String> paramList = new ArrayList<>();
            for(Role role:roles){
                paramList.add(role.getRoleid());
            }
            List<RolePermission> rolePermissions = rolePermissionMapper.selectPermissionByRoleid(paramList);
            List paramList1 = new ArrayList();
            for (RolePermission rolePermission: rolePermissions){
                paramList.add(rolePermission.getPermissionid());
            }
        return permissionMapper.selectPermissionByPermissionid(paramList1);
    }

}
