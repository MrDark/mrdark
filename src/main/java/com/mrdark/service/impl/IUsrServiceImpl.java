package com.mrdark.service.impl;

import com.mrdark.dao.UserMapper;
import com.mrdark.domain.User;
import com.mrdark.service.IUsrService;
import com.mrdark.util.JwtUtil;
import com.mrdark.vo.Rst;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service("IUsrService")
public class IUsrServiceImpl implements IUsrService {
    @Autowired(required = false)
    private UserMapper userMapper;

    @Override
    public Rst loginUser(User user) {
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUserid(), user.getPassword());
        Subject subject = SecurityUtils.getSubject();
        Map rstMap = new HashMap();
        Rst rst = new Rst("0000", "登录成功");
        try {
            subject.login(token);
            String sessionId = subject.getSession().getId().toString();
            log.info("sessionId = " + sessionId);

            //jwt加密sessionId//
            String Token = JwtUtil.encode(new JwtUtil.User(user.getUserid(), user.getUsername(), sessionId), 10000000);
            rstMap.put("token", Token);
            rst.setData(rstMap);


        } catch (UnknownAccountException e) {
            rst.setRst("2000","用户不存在");
        } catch (IncorrectCredentialsException ice) {
            //凭据不正确，例如密码不正确 ...
            rst.setRst("2000","密码不正确");
        } catch (LockedAccountException lae) {
            //用户被锁定，例如管理员把某个用户禁用...
            rst.setRst("2000","用户不存在");
        } catch (ExcessiveAttemptsException eae) {
            //尝试认证次数多余系统指定次数 ...
            rst.setRst("2000","请求次数过多，用户被锁定");
        } catch (AuthenticationException ae) {
            //其他未指定异常
            rst.setRst("2000","无法完成登录"+ ae.toString());
        }
        return rst;
    }

    @Override
    @Transactional
    public Rst addUser(User user) {
        Rst rst = new Rst("0000", "添加成功");
        if (user != null && !"".equals(user.getUserid())) {
            try {
                ByteSource byteSourceSalt = ByteSource.Util.bytes(user.getUserid());
                if (!"".equals(user.getPassword())) {
                    Object passObj = new SimpleHash("MD5", user.getPassword(), byteSourceSalt, 2);
                    user.setPassword(passObj.toString());
                    int cnt = userMapper.insert(user);
                    if (cnt == 0) rst.setRst("2000", "添加失败,添加记录为0");
                } else {
                    rst.setRst("2000", "用户密码不能为空");
                }
            } catch (Exception e) {
                rst.setRst("2000", "添加失败" + e.getMessage());
                rst.setCode("2000");
            }

        }
        return rst;
    }

    @Override
    public Rst deleteUser(String userId) {
        Rst rst = new Rst("0000", "删除成功");
        int cnt = userMapper.deleteUserByUserId(userId);
        if (cnt == 0) rst.setRst("2000", "删除失败");
        return rst;
    }

    @Override
    public Rst updateUser(User user) {
        Rst rst = new Rst("0000", "更新成功");
        int cnt = userMapper.updateUserByUserId(user);
        if (cnt == 0) rst.setRst("2000", "更新失败");
        return rst;
    }

    @Override
    public User selectUserByUserid(String userId){

        return userMapper.selectUserByUserid(userId);
    }



}


