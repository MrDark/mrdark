package com.mrdark.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mrdark.dao.MenuMapper;
import com.mrdark.domain.Menu;
import com.mrdark.domain.Role;
import com.mrdark.service.IMenuService;
import com.mrdark.service.IRoleAndPermissionService;
import com.mrdark.util.StringUtil;
import com.mrdark.vo.MenuVo;
import com.mrdark.vo.Rst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service("IMenuService")
public class IMenuServiceImpl implements IMenuService {
    @Autowired(required = false)
    MenuMapper menuMapper;
    @Autowired
    IRoleAndPermissionService iRoleAndPermissionService;
    @Override
    public Rst selectMenuAndRoute(String userid) {
        log.info("执行：" + this.getClass().getName());
        Rst rst = new Rst("0000","查询成功");
        try {
            List<MenuVo> menuVoList = new ArrayList<>();
            List<MenuVo> menuVos =   menuMapper.selectMenuFatherNode();
            for (MenuVo menuVo:menuVos ){
                menuVo.setMenuVos(Q(menuVo));
                menuVoList.add(menuVo);
            }
            JSONArray array= JSONArray.parseArray(JSON.toJSONString(menuVoList));
            rst.setData(array);

        }catch (Exception e){

            log.info("出现异常" + e.toString());
            rst.setRst("2000","查询出现异常" + e.getMessage());
        }
        return rst;
    }


    /**
     * 递归查询子节点
     * @param menuVo
     * @return
     */
    private List<MenuVo> Q(MenuVo menuVo){

        List<MenuVo> menuVoList = new ArrayList<>();
        List<MenuVo> menuVos =   menuMapper.selectNodeByFatherNodId(menuVo.getMenuid());
        for (MenuVo menuVo1:menuVos ){
            menuVo1.setMenuVos(Q(menuVo1));
            menuVoList.add(menuVo1);
        }
        return menuVoList;
    }
}
