package com.mrdark.service;

import com.mrdark.domain.Permission;
import com.mrdark.domain.Role;

import java.util.List;

public interface IRoleAndPermissionService {
    /**
     * 根据用户名取得role
     * @param userid
     * @return
     */
    List<Role> selectRoleByRoleid(String userid);

    /**
     * 根据角色查询用户权限
     * @param roles
     * @return
     */
    List<Permission> selectPermissionByRole(List<Role> roles);
}
