package com.mrdark.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class PageController {

    @GetMapping("index")
    public String index(){
        return "index";
    }
    @GetMapping("/login")
    public String login(){
        return "login";
    }
    @GetMapping("/chart")
    public String chart(){
        return "chart";
    }
    @GetMapping("/form-amazeui")
    public String fromAmazeui(){
        return "form-amazeui";
    }
    @GetMapping("/form-line")
    public String formLine(){
        return "from-line";
    }
    @GetMapping("/form-new")
    public String fromNew(){
        return "form-new";
    }
    @GetMapping("/form-new-list")
    public String fromNewList(){
        return "form-new-list";
    }
    @GetMapping("/table-font-list")
    public String tableFontList(){
        return "table-font-list";
    }
    @GetMapping("/table-images-list")
    public String tableImagesList(){
        return "table-images-list";
    }
    @GetMapping("/header")
    public String header(){ return "header";}
    @GetMapping("/dashboard")
    public String dashboard(){ return "dashboard";}
}
