package com.mrdark.controller;


import com.mrdark.domain.User;
import com.mrdark.service.IMenuService;
import com.mrdark.service.IRoleAndPermissionService;
import com.mrdark.service.IUsrService;
import com.mrdark.vo.Rst;
import lombok.extern.slf4j.Slf4j;

import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;


@Slf4j
@Controller
public class MainController {
    @Autowired(required = false)
    IUsrService iUsrService;
    @Autowired(required = false)
    IMenuService iMenuService;

    /**
     *登录
     * @param user
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Rst login(@RequestBody User user){

        log.info("登录>>>>>>>>>" + user.toString());
        return iUsrService.loginUser(user);
    }


    @RequestMapping(value = "/getMenu", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    //@RequiresRoles(value={"admin"})
    public Rst getMenu(@RequestBody User user){
        log.info("获取菜单>>>>>>>>>");
        return iMenuService.selectMenuAndRoute(user.getUserid());
    }

    /**
     * 添加用户
     * @param user
     * @return
     */
    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public Rst addUser(User user){
       return iUsrService.addUser(user);
    }


    /**
     * 删除用户
     * @param userId
     * @return
     */
    @RequestMapping(value = "/deleteUser",method = RequestMethod.POST)
    public Rst deleteUser(String userId){
        return iUsrService.deleteUser(userId);
    }

    /**
     * 添加用户
     * @param user
     * @return
     */
    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public Rst updateUser(User user){
        return iUsrService.updateUser(user);
    }



}
