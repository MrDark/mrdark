package com.mrdark;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@ServletComponentScan
@MapperScan({"com.mrdark.dao.*"})
@ComponentScan(basePackages={"com.mrdark.*","com.mrdark.domain"})
@SpringBootApplication
public class MrdarkApplication {

    public static void main(String[] args) {
        SpringApplication.run(MrdarkApplication.class, args);
    }

}
