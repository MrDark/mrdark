package com.mrdark.util;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
@Slf4j
public class JwtUtil {
    /**
     * 密钥
     */
    private static  final String SECRET="MrDar";
    /**
     * 默认字段key:exp
     */
    private static final String EXP="exp";
    /**
     * 默认字段key:payload
     */
    private static final String PAYLOAD="payload";

    /**
     * 加密
     * @param object 加密数据
     * @param maxTime 有效期（毫秒数）
     * @param <T>
     * @return
     */
    public static <T> String encode(T object,long maxTime){
        log.info("token加密开始...");
        try{
            final JWTSigner signer=new JWTSigner(SECRET);
            final Map<String ,Object> data=new HashMap<>(10);
            ObjectMapper objectMapper=new ObjectMapper();
            String jsonString=objectMapper.writeValueAsString(object);
            data.put(PAYLOAD,jsonString);
            data.put(EXP,System.currentTimeMillis()+maxTime);
            return signer.sign(data);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 数据解密
     * @param jwt 解密数据
     * @param tClass 解密类型
     * @param <T>
     * @return
     */
    public static <T> T decode(String jwt,Class<T> tClass){
        log.info("token解密开始....");
        final JWTVerifier jwtVerifier=new JWTVerifier(SECRET);
        try{
            final Map<String,Object> data=jwtVerifier.verify(jwt);
            //判断数据是否超时或者符合标准
            if(data.containsKey(EXP)&&data.containsKey(PAYLOAD)){
                long exp= (long) data.get(EXP);
                long currentTimeMillis=System.currentTimeMillis();
                if(exp>currentTimeMillis){
                    String json= (String) data.get(PAYLOAD);
                    ObjectMapper objectMapper=new ObjectMapper();
                    return objectMapper.readValue(json,tClass);
                }
            }
            return null;
        }catch (Exception e){
            //e.printStackTrace();
            return null;
        }
    }
    public static class  User{
        String id;
        String name;
        String token;
        public User() {
        }
        public User(String id, String name, String token) {
            this.id = id;
            this.name = name;
            this.token = token;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", token='" + token + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) {
//        有效期10秒
//        加密：
        String token=encode(new User("1","张三","riversky"),10000);
        System.out.println(token);
//      解密
        User user=decode(token,User.class);
        System.out.println(user);
    }
}
